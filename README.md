# DevOps FAQ

## Jenkins Ate My Password

[https://www.serverlab.ca/tutorials/linux/administration-linux/how-to-reset-jenkins-admin-users-password/]

but your config file is in `sudo nano ~jenkins/.jenkins/config.xml`

You don't need to do point 6 "Check the Enable Security check box" as it doesn't exist any more

## Docker Ate My Diskspace

`docker image prune`
`docker system prune --all --volumes --force`
`docker builder prune --all`

## Docker Swarm hates networking

[https://github.com/moby/moby/issues/417750]

`ethtool -K <dev> tx-checksum-ip-generic off`


